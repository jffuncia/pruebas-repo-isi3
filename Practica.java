import java.util.List;

public class Practica {
	private String url;
	private String dni;


	/*Constructor de Practica */
	public Practica(String url, String dni){
		this.url = url;
		this.dni = dni;
	}
	
	/*Metodo para obtener url */
	public String getUrl() {
		return url;
	}
	
	/*Metodo para obtener el dni*/
	public String getDni() {
		return dni;
	}
	
	/*Metodo para buscar una practica dada una lista y su url */
	public static boolean searchPractica(String url, List lista){
		int x = 0;
		boolean found = false;
		int dim = lista.size();
		
		
		while(found != true && x < dim){
		Practica n_name = (Practica)lista.get(x);
			if(n_name.getUrl().equals(url)){
				found = true;
			}else{
				x++;
			}
		}
		
		return found;
	}
	
	/*Metodo para dado una lista y un dni, encontrar su practica */
	public static String searchUrl(String dni, List listaPrac, List listaAlum){
		int x = 0;
		boolean found = false;
		String url = null;
		int dim = listaAlum.size();
		
		if(!listaPrac.isEmpty()){
			while(found != true && x < dim){
			Alumno n_name = (Alumno)listaAlum.get(x);
				if(n_name.getDni().equals(dni)){
					found = true;
					Practica pfound = (Practica)listaPrac.get(x);
					url = pfound.getUrl();
				}else{
					x++;
				}	
			}
		}
		
		return url;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Practica other = (Practica) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
}

